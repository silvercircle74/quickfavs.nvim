# Quickfavs.Nvim

A simple plugin to maintain a list of favorite files and folders and open them with a 
[Telescope](https://github.com/nvim-telescope/telescope.nvim) picker or in the 
[Neotree](https://github.com/nvim-neo-tree/neo-tree.nvim) or 
[NvimTree](https://github.com/nvim-tree/nvim-tree.lua) file manager.

# Minimal setup

```lua
use 'https://gitlab.com/silvercircle74/quickfavs.nvim'
require('quickfavs').setup({})
```


Write some docs, fix issues. Release it.

# License: MIT

 (c) Copyright 2025 Alex Shorner <as@subspace.cc>.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
