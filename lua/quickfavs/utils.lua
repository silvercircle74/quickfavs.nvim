local M = {}
-- some library functions
-- pad string left and right to length with fill as fillchar
function M.pad(string, length, fill)
  local len = vim.fn.strcharlen(string)
  local padlen = (length - len) / 2
  if len >= length or padlen < 2 then
    return string
  end
  return string.rep(fill, padlen) .. string .. string.rep(fill, padlen)
end

function M.lpad(string, length, fill)
  local len = vim.fn.strcharlen(string)
  local padlen = (length - len)
  if len >= length or padlen < 2 then
    return string
  end
  return string.rep(fill, padlen) .. string
end

function M.rpad(string, length, fill)
  local len = vim.fn.strcharlen(string)
  local padlen = (length - len)
  if len >= length or padlen < 2 then
    return string
  end
  return string .. string.rep(fill, padlen)
end

function M.string_split(s, delimiter)
  local result = {};
  for match in (s..delimiter):gmatch("(.-)"..delimiter) do
    table.insert(result, match);
  end
  return result;
end

function M.truncate(text, max_length)
  if max_length > 1 and vim.fn.strwidth(text) > max_length then
    return vim.fn.strcharpart(text, 0, max_length - 1) .. "…"
  else
    return text
  end
end

return M

