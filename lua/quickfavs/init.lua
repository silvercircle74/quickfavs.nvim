-- (c) Copyright 2025 Alex Shorner <as@subspace.cc>
--
--  https://gitlab.com/silvercircle74/quickfavs.nvim
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.

-- TODO: review and improve default settings.

local M = {}

local _prompt = " Enter:edit/browse, <leader>o:Oil, <leader>m:Explore, <leader>g:grep, <leader>n:OpenTree <C-d>:CWD)"

local conf = {
  -- the default filename
  filename = vim.fn.stdpath("config") .. "/favs",
  neotree = 'left',
  debug = false,
  -- custom telescope theme, but you can specify your own, e.g.
  -- telescope_theme = require("telescope.themes").get_dropdown,
  fzf_winopts     =  { width = 80, height = 0.8, preview = { hidden="hidden" } },
  watch_files = true,
  filebrowser = 'fzf',
  prompt = _prompt,
  width = 124,
  picker = 'snacks',
  snacks_layout   = {
    preset = "vertical",
    preview = false,
    layout = {
      backdrop = false,
      border = "single",
      box = "vertical",
      width = 120,
      height = 0.4,
      { win = "list",  border = "none" },
      { win = "input", height = 1, border = "top" },
    },
  },
  snacks_keys = {
    ['<leader>g'] = { { 'close', 'grep' }, mode = { 'n', 'i' } },
    ['<leader>n'] = { { 'close', 'tree' }, mode = { 'n', 'i' } },
    ['<leader>f'] = { { 'close', 'files' }, mode = { 'n', 'i' } },
    ['<leader>m'] = { { 'close', 'explorer' }, mode = { 'n', 'i' } },
    ['<leader>o'] = { { 'close', 'oil' }, mode = { 'n', 'i' } },
    ['<c-d>'] = { 'setdir', mode = { 'n', 'i' } }
  },
  explorer_layout = { },
  snacks_picker_cols= {
    type =      { hl = "Type", width = 10 },
    title =     { hl = "String", width = 34 },
    filename =  { hl = "Member", width = 0 }, -- note: the width for the filename is auto-calculated
    filetype =  { hl = "Comment", width = 10 }
  }
}

local function debugnotify(msg)
  if conf.debug == true then
    vim.notify("Quickfavs: " .. msg, vim.log.levels.DEBUG)
  end
end

local function telescope_theme(opts)
  local lopts = opts or {}
  local defaults = require('telescope.themes').get_dropdown({
    borderchars = {
      results = {"─", "│", " ", "│", '┌', '┐', "│", "│"},
      prompt = {"─", "│", "─", "│", "├", "┤", "┘", "└"},
      preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
    },
    sorting_strategy = "ascending",
    layout_strategy = "vertical",
    path_display={smart = true},
    layout_config = {
      width = lopts.width or 80,
      height = lopts.height or 0.8,
      preview_height = lopts.preview_width or 20,
      prompt_position='bottom',
      scroll_speed = 2,
    },
    previewer = false
  })
  if lopts.search_dirs ~= nil then
    lopts.prompt_title = lopts.prompt_title .. ': ' .. lopts.search_dirs[1]
  end
  if lopts.cwd ~= nil then
    lopts.prompt_title = lopts.prompt_title .. ': ' .. lopts.cwd
  end
  if lopts.path ~= nil then
    lopts.prompt_title = lopts.prompt_title .. ': ' .. lopts.path
  end
  return vim.tbl_deep_extend('force', defaults, lopts)
end

local function mini_pick_center(width, height, col_anchor)
  local _ca = col_anchor or 0.5
  if width > 0 and width < 1 then
    width = math.floor(width * vim.o.columns)
  end
  if height > 0 and height < 1 then
    height = math.floor(height * vim.o.lines)
  end
  return {
    anchor = 'NW', height = height, width = width,
    row = math.floor(_ca * (vim.o.lines - height)),
    col = math.floor(0.5 * (vim.o.columns - width))
  }
end

local favs = {}
local watch = nil

function M.setup(opts)
  opts = opts or {}
  if opts.snacks_layout ~= nil then conf.snacks_layout = {} end
  if opts.explorer_layout ~= nil then conf.explorer_layout = {} end
  conf = vim.tbl_deep_extend('force', conf, opts)
  if watch == nil and conf.watch_files == true then
    watch = vim.loop.new_fs_event()
  end
end

local function onChange(cust, _, fname, status)
  if not status.change then
    --source.debugmsg("No status change, do nothing")
    return
  end
  debugnotify("On Change event for: " .. fname .. ', re-reading favorites')
  if watch ~= nil then
    vim.loop.fs_event_stop(watch)
  end
  for k,_ in pairs(favs) do favs[k] = nil end
  M.readFolderFavs(conf.filename, true)
  if watch ~= nil then
    vim.loop.fs_event_start(watch, fname, {foo='bar'}, vim.schedule_wrap(function(...) onChange(cust, ...) end))
  end
end
-- read favorite files and folders from the given file

local __sysname = vim.uv.os_uname().sysname:lower()
local __iswin = not not (__sysname:find('windows') or __sysname:find('mingw'))

local function is_absolute(p)
  return string.match(p, __iswin and "^%a:/" or "/") ~= nil or false
end

function M.readFolderFavs(favfile, forcerescan)
  local utils = require('quickfavs.utils')
  local filename

  -- just return the table if it has already been populated unless a force
  -- rescan is requested.
  if forcerescan == false and #favs > 0 then
    return true, favs
  end

  local fn = vim.fn.expand(vim.fs.normalize(favfile, { win = __iswin } ))

  if is_absolute(fn) then
    filename = fn
  else
    filename = vim.fn.expand(vim.fs.joinpath(vim.fn.stdpath("data"), favfile))
  end

  debugnotify("The quickfavs file is: " .. filename)

  if #favs > 0 then
    for k,_ in pairs(favs) do favs[k] = nil end
  end

  if vim.fn.filereadable(filename) == 0 then
    -- debugnotify("The given file (" .. filename .. ") does not exist")
    return false, {}
  end
  local file = io.open(filename)
  if file == nil then
    debugnotify("Favorite file not found, should be in " .. filename)
    return false, {}
  end
  local lines = file:lines()
  for line in lines do
    if line ~= nil and #line > 1 then
      local elem = utils.string_split(line, '|')
      if #elem == 2 then
        local e = vim.fn.expand(elem[2])
        local ft = vim.filetype.match( { filename = e } )
        if vim.fn.isdirectory(e) == 1 then
          table.insert(favs, { title = elem[1], filename = e, type = "@Dir", ft = ft or 'Unknown' } )
        elseif vim.fn.filereadable(e) == 1 then
          table.insert(favs, { title = elem[1], filename = e, type = "@File", ft = ft or 'Unknown' } )
        end
      end
    end
  end
  io.close(file)
  if watch ~= nil then
    debugnotify("starting file watcher for: " .. filename)
    vim.loop.fs_event_start(watch, filename, {}, vim.schedule_wrap(function(...) onChange(filename, ...) end))
  end
  return true, favs
end

local function open_entry(where, selection)
  local name = nil

  if conf.picker == "telescope" and selection.value.filename ~= nil and #selection.value.filename > 0 then
    name = selection.value.filename
  end
  if conf.picker == "snacks" and selection.file ~= nil and #selection.file > 0 then
    name = selection.file
  end

  if name ~= nil and #name > 0 then
    if vim.fn.filereadable(name) ~= 0 then
      vim.cmd('e ' .. name)
    elseif vim.fn.isdirectory(name) ~= 0 then
      if where == 'tree' then
        local status, api = pcall(require, 'nvim-tree.api')
        if status == true then
          api.tree.change_root(name)
          api.tree.focus()
        else
          vim.cmd("Neotree position=" .. conf.neotree .. " dir=" .. name)
        end
      elseif where == 'mini' then
        require("mini.extra").pickers.explorer({cwd = name}, { window={ config = mini_pick_center(60, 0.6, 0.2) }})
      elseif where == 'files' then
        require("mini.files").open(name)
      elseif where == 'grep' then
        vim.schedule(function() require("fzf-lua").live_grep( { cwd = name, winopts = conf.fzf_winopts } ) end)
      elseif where == 'fzf' then
        require("fzf-lua").files( { formatter = "path.filename_first", cwd = name, winopts = conf.fzf_winopts } )
      elseif where == 'snacks' then
        require("snacks").picker.files( { cwd = name, layout = { preset="vertical" } } )
      elseif where == 'telescope' then
        require("telescope.builtin").find_files(telescope_theme({prompt_title="Browse files", cwd = name}))
      elseif where == "oil" then
        local s, oil = pcall(require, "oil")
        if s then oil.open(name) end
      end
    end
  end
end

function M.Quickfavs()
  if conf.picker == "telescope" then
    local status, _ = pcall(require, "telescope.pickers")
    if status == true then
      return M.Quickfavs_Telescope()
    else
      vim.notify("Telescope plugin not installed")
      return
    end
  elseif conf.picker == "snacks" then
    local status, _ = pcall(require, "snacks.picker")
    if status == true then
      return M.Quickfavs_Snacks()
    else
      vim.notify("Snacks plugin not installed or not active")
      return
    end
  else
    vim.notify("No picker configured")
  end
end
--- open a telescope picker with the favorite files and folders read from the favorite file
-- @param forcerescan boolean: Force a rescan of the favorite file.
-- 'telescope'
function M.Quickfavs_Telescope()
  local max_width = conf.width - 2
  local title_width = 30
  local status
  local lutils = require("quickfavs.utils")

  local filename_width = max_width - title_width - 8 - 5 - 12

  local pickers = require "telescope.pickers"
  local finders = require "telescope.finders"
  local tconf = require("telescope.config").values
  local actions = require "telescope.actions"
  local action_state = require "telescope.actions.state"
  local tutils = require "telescope.utils"
  --  local make_entry = require "telescope.make_entry"

  status, favs = M.readFolderFavs(conf.filename, false)
  if status == false then
    debugnotify("Read favorite folders returned an error")
    return
  end

  -- use telescope
  local favselector = function(opts)
    opts = opts or {}
    pickers.new(opts, {
      prompt_title = "Select directory favorite",
      layout_config = {
        horizontal = {
          prompt_position = "bottom"
        }
      },
      finder = finders.new_table {
        results = favs,
        entry_maker = function(entry)
          local icon, hl_group = tutils.get_devicons(entry.filename, false)
          return {
            value = entry,
            display = function()
              return lutils.truncate(lutils.rpad(entry.type, 8, ' ') .. lutils.rpad(entry.title, title_width, ' ') .. "  " .. icon .. "  " .. lutils.rpad(entry.filename, filename_width, ' ') .. ' ' .. entry.ft, max_width),
                { { { 40, 43 }, hl_group } }
            end,
            ordinal = entry.title .. "  " .. entry.type
          }
        end,
      },
      sorter = tconf.generic_sorter(opts),
      attach_mappings = function(prompt_bufnr, map)
        map('i', '<c-d>', function(_)
          local selection = action_state.get_selected_entry()
          if vim.fn.isdirectory(selection.value.filename) ~= 0 then
            vim.notify("Set " .. selection.value.filename .. " as current working directory.", 3)
            vim.api.nvim_set_current_dir(selection.value.filename)
          end
        end)
        map('i', '<c-q>', function(_) end)    -- remap this to nothing, it will otherwise produce an error
        map('i', '<leader>n', function(_)
          actions.close(prompt_bufnr)
          local selection = action_state.get_selected_entry()
          vim.cmd.stopinsert()
          vim.schedule(function() open_entry('tree', selection) end)
        end)
        map('i', '<leader>m', function(_)
          actions.close(prompt_bufnr)
          local selection = action_state.get_selected_entry()
          vim.cmd.stopinsert()
          vim.schedule(function() open_entry('mini', selection) end)
        end)
        map('i', '<leader>g', function(_)
          actions.close(prompt_bufnr)
          local selection = action_state.get_selected_entry()
          vim.cmd.stopinsert()
          vim.schedule(function() open_entry('grep', selection) end)
        end)
        map('i', '<leader>f', function(_)
          actions.close(prompt_bufnr)
          local selection = action_state.get_selected_entry()
          vim.cmd.stopinsert()
          vim.schedule(function() open_entry('files', selection) end)
        end)
        actions.select_default:replace(function()
          actions.close(prompt_bufnr)
          local selection = action_state.get_selected_entry()
          open_entry(conf.filebrowser, selection)
        end)
      return true
      end,
    }):find()
  end
  favselector(conf.telescope_theme{layout_config={width=conf.width, height=0.4}, prompt_title = conf.prompt})
end

function M.Quickfavs_Snacks()
  local status
  local snacks = require("snacks")
  local lutils = require("quickfavs.utils")
  local align = snacks.picker.util.align

  status, favs = M.readFolderFavs(conf.filename, false)
  if status == false then
    debugnotify("Read favorite folders returned an error")
    return
  end

  if conf.snacks_layout.layout.title == nil then
    conf.snacks_layout.layout.title = _prompt
  end

  local filename_width = conf.snacks_layout.layout.width - conf.snacks_picker_cols.type.width -
      conf.snacks_picker_cols.title.width - conf.snacks_picker_cols.filetype.width - 3 --[[icon]] - 3 --[[col gaps]]
  return snacks.picker({
    finder = function()
      local items = {}
      for i, item in ipairs(favs) do
        table.insert(items, {
          idx = i,
          file = item.filename,
          type = item.type,
          title = item.title,
          ft = item.ft,
          text = item.title .. "  " .. item.filename
        })
      end
      return items
    end,
    focus = "input",
    auto_close = false,
    layout = conf.snacks_layout,
    win = {
      input = {
        keys = conf.snacks_keys
      },
      list = {
        keys = conf.snacks_keys
      }
    },
    actions = {
      oil = function(picker)
        local item = picker:current()
        vim.schedule(function() open_entry("oil", item) end)
      end,
      grep = function(picker)
        local item = picker:current()
        vim.schedule(function() open_entry("grep", item) end)
      end,
      tree = function(picker)
        local item = picker:current()
        vim.schedule(function() open_entry("tree", item) end)
      end,
      setdir = function(picker)
        local item = picker:current()
        if vim.fn.isdirectory(item.file) ~= 0 then
          vim.notify("Set " .. item.file .. " as current working directory.", 3)
          vim.api.nvim_set_current_dir(item.file)
        else
          vim.notify("Selection is not a directory")
        end
      end,
      files = function(picker)
        local item = picker:current()
        vim.schedule(function() open_entry("files", item) end)
      end,
      explorer = function(picker)
        local item = picker:current()
        if item.type == "@Dir" then
          vim.schedule(function() snacks.picker.explorer({cwd = item.file, layout = conf.explorer_layout }) end)
        end
      end
    },
    format = function(item, _)
      local entry = {}
      local icon, icon_hl = snacks.util.icon(item.ft, item.type == "@File" and "filetype" or "directory")
      local type = item.type == "@Dir" and 0 or 1
      local file = lutils.truncate(item.file, filename_width - 1)
      local pos = #entry

      entry[pos + 1] = { align(item.type, conf.snacks_picker_cols.type.width), conf.snacks_picker_cols.type.hl }
      entry[pos + 2] = { align(item.title, conf.snacks_picker_cols.title.width), conf.snacks_picker_cols.title.hl }
      entry[pos + 3] = { align(icon, 3), icon_hl }
      entry[pos + 4] = { " " }
      entry[pos + 5] = { align(file, filename_width), conf.snacks_picker_cols.filename.hl }
      entry[pos + 6] = { align(type == 0 and "folder" or item.ft,
          conf.snacks_picker_cols.filetype.width, { align="right" }), icon_hl }
          -- type == 0 and conf.snacks_picker_cols.filetype.hl or icon_hl }

      return entry
    end,
    confirm = function(picker, item)
      picker:close()
      vim.schedule(function() open_entry(conf.filebrowser, item) end)
    end,
  })
end

return M
